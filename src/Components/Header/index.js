import React, { useState } from 'react';
import styled from '@emotion/styled';
import { Menu, Lock} from '@emotion-icons/material'
import { device } from '../../devices';

const HeaderStyled = styled.header /* css */`
    -moz-align-items: center;
    -webkit-align-items: center;
    -ms-align-items: center;
    align-items: center;
    background: #FFFFFF;
    cursor: default;
    height: 5em;
    left: 0;
    position: fixed;
    top: 0;
    width: 100%;
    z-index: 10001;
    box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.075);
    display: flex;
    justify-content: space-between;
    padding: 0 20px;
    .TabView{
        display: none;
    }
    .DesktopView{
        display: block;
    }
    .Container{
        width: 1200px;
        margin: 0 auto;
        padding: 1em 2em;
        display: flex;
        justify-content: space-between;
        align-items: center;
        color: rgb(0, 191, 255);
        .Logo {
            display: flex;
            align-items: center;
        }
        ul {
            list-style-type: none;
            display: flex;
        }
        li{
            padding: 0 10px;
        }
        li a {
            font-size: 1.2rem;
            cursor: pointer;
            display: block;
            position: relative;
        }

        li a:hover {
            font-weight: bold;
            color: #043759;
        }

        li a.current{
            color: #043759;
        }
    }
    @media only screen and ${device.maxTablet} { 
        height: 4em;
        border-bottom: 1px solid #ddd;
        .DesktopView{
            display: none;
        }
        .Container{
            color: rgb(0, 191, 255);
            justify-content: center;
            align-items: center;
        }
        .TabView{
            display: block;
            nav {
                width: 100%;
                overflow: hidden;
                background-color: #FFFFFF;
                position: absolute;
                top: 4em;
                left: 0;
            }
            ul{
                flex-direction: column;
                padding-inline-start: 0;
                margin-block-start: 0;
                margin-block-end: 0;
            }
            li {
                padding: 0.8em;
                border-bottom: 1px solid #e9e6e4;
                cursor: pointer;
                background-color: #F0F8FF;
            }
            li a {
                text-decoration: none;
                font-size: 17px;
                display: block;
                font-size: 1rem;
            }
            li:hover {
                background-color: #e9e6e4;
                color: black;
            }
        }
    }
`

const Logo = ({width, height}) => {
    return (<svg xmlns="http://www.w3.org/2000/svg" version="1.1" id="Layer_1" x="0px" y="0px" width={width} height={height} viewBox="0 0 500 500" >
        <g>
            <g>	
                <g>
                    <polygon fill="#1C6A89" points="317.279,257.005 274.98,257.005 267.396,242.898 309.697,242.898    "/>
                </g>
            </g>
            <g>	
                <g>
                    <path fill="#1C6A89" d="M222.673,233.263l-89.457-82.64c-12.108-11.189-18.16-24.516-18.16-39.95     c0-15.435,5.22-28.29,15.665-38.588c10.442-10.286,23.38-15.435,38.817-15.435c15.45,0,28.072,4.246,37.912,12.709     c9.843,8.478,14.764,19.825,14.764,34.055h4.088V47.569h-17.717c-14.516-3.027-27.541-4.547-39.046-4.547     c-24.517,0-43.966,7.66-58.341,22.938c-14.375,15.277-21.561,33.667-21.561,55.155c0,21.503,8.177,39.807,24.516,54.942     l87.173,80.819c11.807,10.888,17.702,25.047,17.702,42.446c0,17.4-6.125,31.859-18.39,43.351     c-12.25,11.521-27.915,17.257-46.979,17.257c-18.476,0-33.309-6.123-44.499-18.391c-11.201-12.25-16.796-27.613-16.796-46.074     c0-3.328,0.143-6.656,0.459-10h-4.089c-4.547,13.628-6.814,26.496-6.814,38.604c0,12.106,0.458,20.283,1.363,24.516     c8.175,8.479,19.752,14.747,34.729,18.835c14.991,4.089,29.134,6.14,42.446,6.14c25.735,0,46.32-7.789,61.756-23.383     c15.435-15.592,23.154-35.488,23.154-59.703S237.791,247.193,222.673,233.263z"/>
                </g>
            </g>
            <g>
                <g>
                    <path fill="#1C6A89" d="M403.915,384.954l-47.649-124.197l31.323-11.835c0,0-14.07,20.989,18.562,60.708     c0,0,19.065,14.24,7.74,43.209C413.892,352.839,402.693,378.254,403.915,384.954"/>
                </g>
            </g>
            <g>
                <g>
                    <path fill="#1C6A89" d="M312.343,46.134h-32.399l73.877,210.002l29.643-11.936L312.343,46.134z M327.113,168.718l22.11-8.535     l1.593,4.59l-22.114,8.536L327.113,168.718z M329.57,175.805l22.101-8.521l1.591,4.577l-22.113,8.534L329.57,175.805z      M331.843,182.374l22.114-8.52l1.589,4.575l-22.114,8.536L331.843,182.374z M335.877,194.051l-1.589-4.591l22.114-8.521     l1.589,4.577L335.877,194.051z"/>
                </g>
            </g>
        </g>
        <text transform="matrix(1 0 0 1 15.0039 448.4014)" fill="#050505" font-family="'BigCaslon-Medium'" font-size="28.5089" letter-spacing="10">SCALPEL ACADEMY</text>
    </svg>
    )
}

export const Header = ({}) => {
    const [ showMenu, setMenuState ] = useState(false); 
    return (
        <HeaderStyled>
            <section className="TabView">
                <Menu size="36" color="#043759" onClick={() => setMenuState(!showMenu)}/>
                {showMenu && <nav>
                    <ul>
                        <li><a className="anchor" href="#classroom" onClick={() => setMenuState(!showMenu)}>Classroom Program</a></li>
                        <li><a className="anchor" href="#personlised" onClick={() => setMenuState(!showMenu)}>Personlised Focus</a></li>
                        <li><a className="anchor" href="#material" onClick={() => setMenuState(!showMenu)}>Sample Study material</a></li>
                    </ul>
                </nav>}
            </section>
            <section className="Container">
                <div className="Logo">
                    <Logo width="60px" height="60px"/>
                </div>
                <nav className="DesktopView">
                    <ul>
                        <li><a className="anchor" href="#classroom">Classroom Program</a></li>
                        <li><a className="anchor" href="#personlised">Personlised Focus</a></li>
                    </ul>
                </nav>
            </section>
		</HeaderStyled>
    )
}