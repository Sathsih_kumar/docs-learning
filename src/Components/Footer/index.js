import React from 'react';
import styled from '@emotion/styled';
import { device } from '../../devices';

const FooterStyled = styled.footer /* css */`
	display: flex;
	align-items: center;
	justify-content: space-between;
	padding: 1em;
	border-top: 1px solid #DDD;
	.list {
		display: flex;
		div{
			padding:5px 10px 5px 0;
		}
	}
	.Rights{
		padding: 5px 0 5px 10px;
	}
	@media only screen and ${device.desktop} { 
		width: 1200px;
		margin: 0 auto;
	}
	@media only screen and ${device.maxTablet} { 
		width: auto;
		flex-direction: column;
	}
	@media only screen and ${device.maxMobileL} { 
		.list {
			display: flex;
			flex-wrap: wrap;
			justify-content: center;
		}
	}
`

export const Footer = ({}) => {
    return (
        <FooterStyled>
			<section className="list">
				<div>
					<a href="/disclaimer/">Disclaimer</a>
				</div>
				<div>
					<a href="/privacy-policy/">Privacy Policy</a>
				</div>
				<div>
					<a href="/terms-of-services/">Terms of Services</a>
				</div>
				<div>
					<a href="/sitemap.xml">Sitemap</a>
				</div>
			</section>
			<div className="Rights">©&nbsp;2020, SCALPEL ACCADEMY. All rights reserved.</div>
		</FooterStyled>
    )
}