export const getOffset = (element) => {
      if (!element.getClientRects().length)
      {
        return { top: 0, left: 0 };
      }

      let rect = element.getBoundingClientRect();
      let win = element.ownerDocument.defaultView;
      return (
      {
        top: rect.top + win.pageYOffset,
        left: rect.left + win.pageXOffset
      });   
}

export const validateEmail = email => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

export const validateMobile = number => {
    const re = /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/
    return re.test(number)
}