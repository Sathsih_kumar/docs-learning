import React from 'react';
import styled from '@emotion/styled';
import { BannerSection } from './BannerSection';
import { ClassroomProgram } from './ClassroomProgram';
import { StateOfArch } from './StateOfArch';
import { WhyScalpel } from './WhyScalpel';
import { ViewSamples } from './ViewSamples';
import { device } from '../../../devices';

const HomePageSectionStyled = styled.section`
    display: flex;
    flex-direction: column;
    justify-content: center;
    font-family: 'Roboto', sans-serif;
    margin-top: 5em;
    @media only screen and ${device.maxTablet} { 
        margin-top: 4em;
    }
`

export const HomePageSection = ({}) => {
    return (
        <HomePageSectionStyled>
            <BannerSection/>
            <ClassroomProgram/>
            <StateOfArch/>
            <WhyScalpel/>
            {/* <ViewSamples/> */}
		</HomePageSectionStyled>
    )
}