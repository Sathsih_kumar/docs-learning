import React from 'react';
import styled from '@emotion/styled';
import { device } from '../../../devices';
import * as SA from './SA.png';

const ClassroomProgramStyled = styled.section/* css */`
    display: flex;
    flex-direction: column;
    color: #333333;
    .Program__Container{
        margin: 0 auto;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
    }
    .Classroom__Program__Image__Container{
        font-size: 2rem;
        text-transform: uppercase;
        line-height: 1.3;
        font-weight: 700;
        text-align: center;
        margin: 2em;
        color: #043759;
    }

    .Classroom__Program__Info{
        display: flex;
        padding: 0 2em;
        .Classroom__Video__Content{
            width: 100%;
            margin-bottom: 3em;
            .Title{
                font-size: 1.8rem;
                font-weight: bold;
                text-transform: uppercase;
                line-height: 1.3;
                color: #043759;
            }
            .Info{
                .List{
                    padding-inline-start: 0;
                }
                ul{
                    list-style: none;
                    line-height: 2;
                }
                ul li:before {
                    content: '✓';
                }
            }
        }
    }

    @media only screen and ${device.desktop} { 
        .Program__Container{
            width: 1200px;
        }
        .Classroom__Program__Image__Container{
            width: 100%
        }
        .Classroom__Program__Info{
            width: 100%;
        }
    }

    @media only screen and ${device.maxTablet} { 
        .Program__Container{
            flex-direction: column;
        }
    }

    @media only screen and ${device.maxMobileL} { 
        .Classroom__Program__Info{
            flex-direction: column;
            align-items: center;
            .Classroom__Video__Content{
                margin-bottom: 1.5em;
                .Title{
                    font-size: 1.2rem;
                }
            }
        }
        .Classroom__Program__Image__Container{
            font-size: 1.5rem;
        }
    }
`

export const ClassroomProgram = ({}) => {
    return (
        <ClassroomProgramStyled id="classroom">
            <section className="Program__Container">
                <section className="Classroom__Program__Image__Container">
                    Best online app based classroom program
                </section>
                <section className="Classroom__Program__Info">
                    <section className="Classroom__Video__Content">
                        <div className="content">
                            <div className="description">
                                <div className="Title">Video Lectures</div>
                                <div className="Info">
                                    <ul className="List">
                                        <li>
                                            Subject wise 
                                            <ul>
                                                <li>MCQ Discussion</li>
                                                <li>Revision videos</li>
                                            </ul>
                                        </li>
                                        <li>Hardcopy & Softcopy notes</li>
                                        <li>Conceptual & Result oriented</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="Classroom__Video__Content">
                        <div className="content">
                            <div className="description">
                                <div className="Title">Question Bank</div>
                                <div className="Info">
                                    <ul className="List">
                                        <li>Daily practice problems</li>
                                        <li>400+ MCQ Problems & theory based questions</li>
                                        <li>Includes previous years questions papers</li>
                                        <li>Create custom MCQ modules to revise a weak topics</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="Classroom__Video__Content">
                        <div className="content">
                            <div className="description">
                                <div className="Title">Be a part of PAN india test</div>
                                <div className="Info">
                                    <ul className="List">
                                        <li>Conducted every month</li>
                                        <li>Performance analysis & improvement tips</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                </section>
            </section>
		</ClassroomProgramStyled>
    )
}