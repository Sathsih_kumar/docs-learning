import React from 'react';
import styled from '@emotion/styled';
import { device } from '../../../devices';
import * as SA from './SA.png';

const StateOfArchStyled = styled.section/* css */`
    display: flex;
    flex-direction: column;
    background: #E9E6E4;
    color: #333333;
    .Arch__Container{
        margin: 0 auto;
        display: flex;
        justify-content: space-between;
    }
    .Arch__Image__Container{
        padding: 2em;
        .Title{
            font-size: 2rem;
            text-transform: uppercase;
            line-height: 1.3;
            font-weight: 700;
            color: #043759;
        }
        .Split{
            width: 5rem;
            height: 0.5rem;
            background-color: #043759;
            margin-bottom: 5px;
        }
        .SubTitle{
            padding: 0.7em 0;
        }
        .responsiveImg{
            max-width: 100%;
            height: auto;
        }
    }
    .Arch__Info{
        padding: 2em;
        margin-top: 5rem;
        .State__Arch__Content{
            width: 100%;
            margin-bottom: 3em;
            .Title{
                font-size: 1.8rem;
                font-weight: bold;
                line-height: 1.3;
                color: #043759;
            }
            .Info{
                padding: 1em 0;
            }
        }
    }
    @media only screen and ${device.desktop} { 
        .Arch__Container{
            width: 1200px;
        }
        .Arch__Image__Container{
            width: 50%;
        }
        .Arch__Info{
            width: 50%;
        }
    }

    @media only screen and ${device.laptop} {
        .Arch__Info{
            .State__Arch__Content{
                margin-bottom: 1.5em;
            }
        }
    }

    @media only screen and ${device.maxTablet} { 
        .Arch__Container{
            width: auto;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
        .Arch__Info{
            margin-top: 0;
            .State__Arch__Content{
                margin-bottom: 1.5em;
                .Title{
                    font-size: 1.2rem;
                    text-align: center;
                }
                .Info{
                    text-align: center;
                }
            }
        }
        .Arch__Image__Container{
            width: auto;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            .Title{
                font-size: 1.5em;
            }
        }
    }
`
    
export const StateOfArch = ({}) => {
    return (
        <StateOfArchStyled id="personlised">
            <section className="Arch__Container">
                <section className="Arch__Image__Container">
                    <div className="Split"></div>
                    <div className="Title">State of Arch</div>
                    <div className="SubTitle"></div>
                    <img src={SA} className="responsiveImg" widht="444" height="444"/>
                </section>
                <section className="Arch__Info">
                    <section className="State__Arch__Content">
                        <div className="content">
                            <div className="description">
                                <div className="Title">Extra personalised classes by expert faculty to work on your weaker topics</div>
                                <div className="Info">Performance analysis that gives customer improvement tips to help them get better and get personal attention and interaction with mentors and faculty instantaneously through social media </div>
                            </div>
                        </div>
                    </section>
                    <section className="State__Arch__Content">
                        <div className="content">
                            <div className="description">
                                <div className="Title">High quality elaborate video lessons to revise anytime, anywhere</div>
                                <div className="Info">Error free high quality videos anywhere anytime. Scalpel Accademy MCQ and video lectures are structured to understood important concepts in each topic. Repeat MCQ until you get them correct.</div>
                            </div>
                        </div>
                    </section>
                    <section className="State__Arch__Content">
                        <div className="content">
                            <div className="description">
                                <div className="Title">Quick doubt resolution</div>
                                <div className="Info">Faculty support and doubts answered by experts in 24 working hours.</div>
                            </div>
                        </div>
                    </section>
                    
                </section>
            </section>
		</StateOfArchStyled>
    )
}