import React from 'react';
import styled from '@emotion/styled';
import { device } from '../../../devices';


const WhyScalpelStyled = styled.section/* css */`
    display: flex;
    flex-direction: column;
    color: #333333;
    .Why__Container{
        margin: 0 auto;
        display: flex;
        justify-content: space-between;
    }
    .Why__Container__Contents{
        width: 100%;
        margin: 4em 2em;
        .Why__Choose__Scapel{
            font-size: 2rem;
            text-transform: uppercase;
            line-height: 1.3;
            font-weight: 700;
            text-align: center;
            margin-bottom: 2em;
            color: #043759;
        }
    }
    @media only screen and ${device.desktop} { 
        .Why__Container{
            width: 1200px;
        }
    }
    @media only screen and ${device.maxTablet} { 
        .Why__Container__Contents{
            .Why__Choose__Scapel{
                font-size: 1.5rem;
            }
        }
    }
`
    
export const WhyScalpel = ({}) => {
    return (
        <WhyScalpelStyled>
            <section className="Why__Container">
                <div className="Why__Container__Contents">
                    <section className="Why__Choose__Scapel">
                        Why To Choose scapel academy ?
                    </section>
                    <div className="ui stackable two column grid">
                        <div className="column">
                            <div className="ui raised segment">
                                <a className="ui ribbon label">Why ?</a>
                                <span><b>Comphrehensive study material</b> and detail Hardcopy and Softcopy of notes to assist studying and make revison faster</span>
                            </div>
                        </div>
                        <div className="column">
                            <div className="ui raised segment">
                                <a className="ui ribbon label">Why ?</a>
                                <span>Adaptive and unlimited <b>customisable mock tests</b> and chapter wise tests and subject with test</span>
                            </div>
                        </div>
                        <div className="column">
                            <div className="ui raised segment">
                                <a className="ui ribbon label">Why ?</a>
                                <span><b>Individual guidance</b> from mentors</span>
                            </div>
                        </div>
                        <div className="column">
                            <div className="ui raised segment">
                                <a className="ui ribbon label">Why ?</a>
                                <span>Guest lectures <b>By top doctors</b> to guide and motivate</span>
                            </div>
                        </div>
                        <div className="column">
                            <div className="ui raised segment">
                                <a className="ui ribbon label">Why ?</a>
                                <span>Video solutions of <b>Daily Practice Problems (DPPs) and Practice Sheets</b> to improve your conceptual understanding and performance analysis to improve weak topics and revision</span>
                            </div>
                        </div>
                    
                    </div>
                </div>
            </section>
		</WhyScalpelStyled>
    )
}