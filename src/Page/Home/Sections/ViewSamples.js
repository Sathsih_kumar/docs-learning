import React from 'react';
import styled from '@emotion/styled';
import { device } from '../../../devices';

const PDFSvg = _ => {
    return <svg xmlns="http://www.w3.org/2000/svg" width="53.82" height="66.171" viewBox="0 0 53.82 66.171">
    <g id="g899" transform="translate(0 0)">
      <g id="g876" transform="translate(0 0)">
        <path id="path890" d="M-46.714,183.976a6.845,6.845,0,0,0-6.834,6.836v52.5a6.845,6.845,0,0,0,6.834,6.836H-6.57A6.845,6.845,0,0,0,.264,243.31V200.142a5.659,5.659,0,0,0-.441-2.494,6.911,6.911,0,0,0-1.353-1.956l-.006-.009-9.96-9.769-.017-.017a7.88,7.88,0,0,0-2.111-1.351,7.714,7.714,0,0,0-3.014-.57l.023,0Z" transform="translate(53.548 -183.976)" fill="#ff2116"/>
        <path id="rect2684" d="M-45.817,187.118H-15.7a6.414,6.414,0,0,1,2.084.389,5.712,5.712,0,0,1,1.452.918l0,0,9.935,9.746a5.482,5.482,0,0,1,.889,1.282,4.868,4.868,0,0,1,.249,1.485q0,.024,0,.048v43.216a4.554,4.554,0,0,1-4.588,4.59H-45.817a4.554,4.554,0,0,1-4.588-4.59v-52.5a4.554,4.554,0,0,1,4.588-4.591Z" transform="translate(52.651 -184.873)" fill="#f5f5f5"/>
        <path id="path2697" d="M-34.969,228.465c-1.545-1.545.127-3.668,4.663-5.922l2.854-1.418,1.112-2.433c.612-1.338,1.524-3.522,2.028-4.852l.916-2.419-.632-1.79c-.777-2.2-1.053-5.509-.561-6.7.667-1.611,2.853-1.446,3.719.281.676,1.349.607,3.792-.194,6.872l-.657,2.526.579.982a34.081,34.081,0,0,0,2.066,2.85l1.537,1.912,1.913-.25c6.076-.793,8.157.555,8.157,2.487,0,2.439-4.771,2.64-8.778-.174a12.168,12.168,0,0,1-1.521-1.262s-2.51.511-3.746.844c-1.276.344-1.912.559-3.781,1.19,0,0-.656.952-1.083,1.644-1.589,2.575-3.445,4.709-4.771,5.486C-32.63,229.189-34.185,229.249-34.969,228.465Zm2.425-.866a18.283,18.283,0,0,0,3.843-4.545l.493-.781-2.243,1.128c-3.465,1.742-5.05,3.384-4.225,4.377.463.558,1.017.512,2.133-.178Zm22.5-6.317a1.3,1.3,0,0,0-.234-2.277c-.747-.376-1.35-.453-3.291-.425-1.193.081-3.112.322-3.437.395,0,0,1.054.728,1.522,1a20.222,20.222,0,0,0,3.242,1.354c1.09.334,1.721.3,2.2-.043Zm-9.049-3.761a22.2,22.2,0,0,1-1.943-2.506,13.967,13.967,0,0,1-1.089-1.622s-.53,1.7-.965,2.73l-1.356,3.352-.393.76s2.091-.685,3.154-.963c1.127-.294,3.413-.744,3.413-.744ZM-22,205.828a4.911,4.911,0,0,0-.167-2.753c-.981-1.072-2.164-.178-1.964,2.371a17.385,17.385,0,0,0,.565,3.226l.518,1.643.364-1.237A32.465,32.465,0,0,0-22,205.828Z" transform="translate(48.405 -189.068)" fill="#ff2116"/>
        <g id="g858" transform="translate(16.064 46.553)">
          <path id="path845" d="M-31.067,249.125h2.508a6.817,6.817,0,0,1,1.952.23,2.291,2.291,0,0,1,1.262,1,3.293,3.293,0,0,1,.512,1.863,3.387,3.387,0,0,1-.416,1.722,2.44,2.44,0,0,1-1.121,1.039,5.459,5.459,0,0,1-2.152.319h-.868v3.955h-1.677Zm1.677,1.3v3.532h.831a2.188,2.188,0,0,0,1.536-.416,1.814,1.814,0,0,0,.43-1.351,2.027,2.027,0,0,0-.282-1.128,1.156,1.156,0,0,0-.623-.534,3.9,3.9,0,0,0-1.061-.1Z" transform="translate(31.067 -249.125)" fill="#2c2c2c"/>
          <path id="path847" d="M-20.547,249.125h2.278a5.15,5.15,0,0,1,2.642.586,3.646,3.646,0,0,1,1.5,1.744,6.279,6.279,0,0,1,.512,2.568,7.168,7.168,0,0,1-.46,2.649,4.213,4.213,0,0,1-1.4,1.87,4.337,4.337,0,0,1-2.672.712h-2.4Zm1.677,1.343v7.443h.7a2.349,2.349,0,0,0,2.122-1.009,4.911,4.911,0,0,0,.66-2.709q0-3.725-2.783-3.725Z" transform="translate(28.064 -249.125)" fill="#2c2c2c"/>
          <path id="path849" d="M-8.593,249.125h5.625v1.343H-6.915V253.5h3.161v1.343H-6.915v4.416H-8.593Z" transform="translate(24.652 -249.125)" fill="#2c2c2c"/>
        </g>
      </g>
    </g>
  </svg>
}

const ViewSamplesStyled = styled.section/* css */`
    display: flex;
    flex-direction: column;
    color: #333333;
    background: #F0F8FF;
    .View__Sample__Container{
        margin: 0 auto;
        display: flex;
        justify-content: space-between;
    }
    .View__Sample__Contents{
        width: 100%;
        margin: 4em 2em;
        display: flex;
        flex-direction: column;
        align-items: center;
        .View__Sample__Title{
            font-size: 2rem;
            text-transform: uppercase;
            line-height: 1.3;
            font-weight: 700;
            text-align: center;
            margin-bottom: 2em;
            color: #043759;
            width: 75%;
        }
        .ui .items-center{
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .ui .pdf{
            .header{
                text-align: center;
                span{
                    color: #043759;
                    font-weight: bold;
                }
            }
            .Downloads{
                margin-top: 1em;
            }
        }
    }
    @media only screen and ${device.desktop} { 
        .View__Sample__Container{
            width: 1200px;
        }
    }
    @media only screen and ${device.maxTablet} { 
        .View__Sample__Contents{
            .View__Sample__Title{
                font-size: 1.5rem;
            }
        }
    }
`

const DownloadStyled = styled.section`
    display: flex;
    background-color: #ff5252;
    border-radius: 0.3rem 1rem 1rem 0.3rem;
    color: #fff;
    font-size: 1.5rem;
    max-width: 16rem;
    margin-left: auto;
    margin-right: auto;
    align-items: center;
    margin-bottom: 1.5em;
    cursor: pointer;
`

const Download = () => {
    return <DownloadStyled>
        <PDFSvg/> Download Here
    </DownloadStyled>
}
    
export const ViewSamples = ({}) => {
    return (
        <ViewSamplesStyled id="material">
            <section className="View__Sample__Container">
                <div className="View__Sample__Contents">
                    <section className="View__Sample__Title">
                        LEARN AND PRACTICE THROUGH OUR REGULAR CLASSES, DPPs AND MORE
                    </section>
                    <div className="ui stackable two column grid">
                        <div className="column items-center">
                            <div className="ui card pdf">
                                <div className="content">
                                    <div className="header">View Sample <span>Videos</span></div>
                                </div>
                                <div className="content">
                                    <div className="Downloads">
                                        <Download/>
                                        <Download/>
                                        <Download/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="column items-center">
                            <div className="ui card pdf">
                                <div className="content">
                                    <div className="header">View Sample <span>DPP + Worksheet</span></div>
                                </div>
                                <div className="content">
                                    <div className="Downloads">
                                        <Download/>
                                        <Download/>
                                        <Download/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
		</ViewSamplesStyled>
    )
}