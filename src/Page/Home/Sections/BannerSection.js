import React, { useState } from 'react';
import styled from '@emotion/styled';
import { Form, Input, Dropdown, Button } from 'semantic-ui-react';
import { device } from '../../../devices';
import { validateEmail, validateMobile } from '../../../utils';

const BannerSectionStyled = styled.section/* css */`
    display: flex;
    flex-direction: column;
    background: #00BFFF;
    .Banner__Container{
        margin: 0 auto;
        color: #FFFAFA;
        display: flex;
        justify-content: space-between;
    }
    .Info__Container{
        padding: 4em 2em;
        h1{
            font-size: 3.5rem;
            margin-top: 0;
            margin-bottom: 2rem;
            font-family: 'Roboto', sans-serif;
            span{
                font-size: 2.5rem;
            }
        }
        .Banner__Aspirations{
            .Title{
                font-size: 2rem;
                text-transform: uppercase;
                line-height: 1;
            }
            .Subtitle{
                font-size: 1.3rem;
                padding: 1em 0;
            }
        }
    }
    .Enquiry__Form{
        padding: 2em;
        background: #FFFFFF;
        border-radius: 5px;
        margin: 4em 2em;
        color: #555555;
        .Title{
            display: flex;
            flex-direction: column;
            align-items: center;
            padding-bottom: 1.5em;
            font-size: 1.5rem;
            b {
                margin-bottom: 5px;
            }
            p {
                text-align: center;
                font-size: 1.5rem;
            }
        }
    }

    @media only screen and ${device.desktop} { 
        .Banner__Container{
            width: 1200px;
        }
        .Info__Container {
            width: 60%;
        }
        .Enquiry__Form {
            width: 40%;
        }
    }

    @media only screen and ${device.maxTablet} { 
        .Banner__Container{
            flex-direction: column;
        }
        .No__Mobile{
            display: none;
        }
        .Info__Container{
            padding: 2em;
            h1{
                font-size: 2.5rem;
                margin-bottom: 0;
                span{
                    font-size: 1.5rem;
                }
            }
        }
        .Banner__Aspirations{
            .Title{
                font-size: 1rem;
            }
            ..Subtitle{
                font-size: 0.7rem;
            }
        }
        .Enquiry__Form{
            margin: 0 2em 2em 2em;
        }
    }
`

const standardOptions = [
    { key: '10', text: 'Class 10', value: 'class10' },
    { key: '11', text: 'Class 11', value: 'class11' },
    { key: '23', text: 'Class 12', value: 'class12' },
]

const dataSkeleton = {
    name: {
        value: '',
        isRequired: true,
        error: false
    },
    email: {
        value: '',
        isRequired: true,
        error: false
    },
    mobile: {
        value: '',
        isRequired: true,
        error: false
    },
    city: {
        value: '',
        isRequired: true,
        error: false
    },
    standard: {
        value: 'class10',
        isRequired: true,
        error: false
    },
    query: {
        value: '',
    }
}

const InputLeftCornerLabeled = props => <Input label={{ icon: 'asterisk' }} labelPosition='right corner' {...props}/>
const DropdownLeftCornerLabeled = props => <Dropdown clearable {...props}/>

export const BannerSection = ({}) => {
    const [ formData, setFormData ] = useState(dataSkeleton);
    const { name, email, mobile, city, standard, query } = formData;
    const onChangeFormData = (e, data, key, errorFn) => {
        const { value } = data;
        const isRequiredField = formData[key].isRequired;
        const executeError = () => {
            if(!isRequiredField){
                return false
            }
            if(errorFn){
                return errorFn()
            }
            return  value.length === 0
        } 
        const newFormData = Object.assign({}, formData, {
            [key]: {
                value,
                isRequired: formData[key],
                error: executeError()
            }
        })
        setFormData(newFormData)
    }
    const enquireNow = async () => {
        const data = Object.keys(formData).reduce((agg, key) => Object.assign(agg, { [key]: formData[key].value }), {});
        try{
            const response = await fetch("https://us-central1-doctors-learning.cloudfunctions.net/enquires/add", { 
                method: "POST", 
                body: JSON.stringify(data),
                headers: {
                    "Content-Type": "application/json"
                }
            });
            if(response.ok){
                const json = await response.json();
                if(json){
                    alert('Enquiry Submitted!!! Our People will reach you shorly');
                    setFormData(dataSkeleton);
                }
            }else{
                alert('Something went wrong please try again!');
            }
        }catch(e){
            alert('Something went wrong please try again!');
        }
    }
    return (
        <BannerSectionStyled>
            <section className="Banner__Container">
                <section className="Info__Container">
                    <h1><span>India's Most Affordable</span><br/>NEET Classes</h1>
                    <div className="Banner__Aspirations">
                        <div className="Title No__Mobile">Dear white coat Aspirants</div>
                        <div className="Subtitle No__Mobile">Get ready to be additcted to learning. India Affordable app based teaching with top family at your finger tips </div>
                        <div className="Subtitle">Get ready for <b>NEET 2021</b> with the most <b>exhaustive</b> and <b>comprehensive</b> online  classroom  program</div>
                    </div>
                </section>
                <section className="Enquiry__Form">
                    <div className="Title">
                        <b>NEET Classes</b>
                        <p>The only online classroom program you need to ace NEET 2021 </p>
                    </div>
                    <Form>
                        <Form.Field
                            id='form-input-control-student-name'
                            control={InputLeftCornerLabeled}
                            placeholder='Student name'
                            value={name.value}
                            onChange={(e,data) => onChangeFormData(e, data, 'name')}
                            error={name.error}
                        />
                        <Form.Field
                            id='form-input-control-email-id'
                            control={InputLeftCornerLabeled}
                            placeholder='Email ID'
                            value={email.value}
                            onChange={(e,data) => onChangeFormData(e, data, 'email', () => !validateEmail(data.value))}
                            error={email.error}
                        />
                        <Form.Field
                            id='form-input-control-mobile-number'
                            control={InputLeftCornerLabeled}
                            placeholder='Mobile Number'
                            value={mobile.value}
                            onChange={(e,data) => onChangeFormData(e, data, 'mobile', () => !validateMobile(data.value))}
                            error={mobile.error}
                        />
                        <Form.Field
                            id='form-input-control-city'
                            control={InputLeftCornerLabeled}
                            placeholder='City'
                            value={city.value}
                            onChange={(e,data) => onChangeFormData(e, data, 'city')}
                            error={city.error}
                        />
                        <Form.Field
                            control={DropdownLeftCornerLabeled}
                            options={standardOptions}
                            placeholder='Current Class'
                            name="standard"
                            selection
                            fluid
                            value={standard.value}
                            onChange={(e,data) => onChangeFormData(e, data, 'standard')}
                            error={standard.error}
                        />
                        <Form.Field
                            id='form-input-control-query'
                            control={Input}
                            placeholder='Query'
                            value={query.value}
                            onChange={(e,data) => onChangeFormData(e, data, 'query')}
                        />
                        <Form.Field
                            id='form-button-control-enquiry'
                            control={Button}
                            content='Enquiry Now'
                            onClick={_ => enquireNow()}
                        />
                    </Form>
                </section>
            </section>
		</BannerSectionStyled>
    )
}