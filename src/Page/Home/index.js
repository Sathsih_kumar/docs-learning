import React, { useEffect, useCallback } from 'react';
import styled from '@emotion/styled';
import { Header } from '../../Components/Header';
import { Footer } from '../../Components/Footer';
import { HomePageSection } from './Sections';
import { getOffset } from '../../utils';

const HomeStyled = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
`

const HomePage = () => {

  function scrollCallback(event){
      const mainNavLinks = document.querySelectorAll("nav ul li a");
      const headerHeight = document.querySelector('header').offsetHeight;
      let fromTop = window.scrollY + headerHeight;
      mainNavLinks.forEach(link => {
        if(link.hash){
          let section = document.querySelector(link.hash);
          if (
            section.offsetTop <= fromTop &&
            section.offsetTop + section.offsetHeight > fromTop
          ) {
            link.classList.add("current");
          } else {
            link.classList.remove("current");
          }
        }
      });
  }

  function hasChangeCallback(){
    const hash = window.location.hash;
    if(hash){
      const element = document.querySelector(hash);
      const headerOffset = document.querySelector('header').offsetHeight;
      const top = getOffset(element).top - headerOffset;
      setTimeout(window.scroll({
        top,
        behavior: 'smooth'
      }), 100)
    }
  }
  
  
  useEffect(() => {
    hasChangeCallback();
    window.addEventListener('scroll', scrollCallback);
    window.addEventListener('hashchange', hasChangeCallback)
    return () => {
      window.removeEventListener('scroll', scrollCallback);
      window.removeEventListener('hashchange', hasChangeCallback)
    };
  })

  return (
    <HomeStyled>
      <Header/>
      <HomePageSection/>
      <Footer/>
    </HomeStyled>
  );
}

export default HomePage;
