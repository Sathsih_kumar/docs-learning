import React from 'react';
import styled from '@emotion/styled';
import HomePage from './Page/Home';
import 'semantic-ui-css/semantic.min.css'

const AppStyled = styled.div`
  display: flex;
  width: 100%;
`

function App() {
  return (
    <AppStyled>
      <HomePage/>
    </AppStyled>
  );
}

export default App;
