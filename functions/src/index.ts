import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as express from 'express';
import * as cors from 'cors';

admin.initializeApp();

const app = express();
app.use(cors({ origin: true }));

app.get('/all', async (req, res) => {
  try{
    const docs  = await admin.firestore().collection('enquires').get();
    if(docs){
      let data: any = []
      docs.forEach(doc => {
        data = [...data, doc.data()]
      })
      res.send(data);
    }else{
      res.status(500).send({ message: 'No docs reference found', error: null })
    }
  }catch(e){
    res.status(500).send({ message: 'Unhandled exception', error: e })
  }
});

app.post('/add', async (req, res) => {
  try{
    const docRef  = await admin.firestore().collection('enquires').doc();
    if(docRef){
      const status = await docRef.set(req.body);
      res.send(status);
    }else{
      res.status(500).send({ message: 'Reference not found', error: null })
    }
  }catch(e){
    res.status(500).send({ message: 'Unhandled exception', error: e })
  }
})

export const enquires = functions.https.onRequest(app);


